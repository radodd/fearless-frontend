window.addEventListener('DOMContentLoaded', async () => {
    const selectTag = document.getElementById('conference');

    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();

      for (let conference of data.conferences) {
        const option = document.createElement('option');
        option.value = conference.href;
        option.innerHTML = conference.name;
        selectTag.appendChild(option);
      }

        selectTag.classList.remove('d-none');

        const loadingIconContainer = document.getElementById('loading-conference-spinner');
        loadingIconContainer.classList.add('d-none');

        const formTag = document.getElementById('create-attendee-form');
        formTag.addEventListener('submit', async event => {
            event.preventDefault();
            const formData = new FormData(formTag);
            const json = JSON.stringify(Object.fromEntries(formData));

            const attendeeUrl = 'http://localhost:8001/api/attendees/';
            const fetchConfig = {
                method: "post",
                body: json,
                credentails: 'include',
                headers: {
                    'Content-Type': 'application/json',
                },
            };
            const response = await fetch(attendeeUrl, fetchConfig);
            if (response.ok) {


                const successElement = document.getElementById('success-message');
                successElement.classList.remove('d-none');

                const attendeeForm = document.getElementById('create-attendee-form');
                attendeeForm.classList.add('d-none');


            }
        });

      }
    });
