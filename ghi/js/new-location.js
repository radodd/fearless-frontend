
// function createCard(name, description, pictureUrl, startDate, endDate, locationDetail) {
//     return `
//     <div class="col">
//       <div class="card">
//         <img src="${pictureUrl}" class="card-img-top">
//         <div class="card-body">
//           <h5 class="card-title">${name}</h5>
//           <p class="text-body-secondary text-muted">${locationDetail}</p>
//           <p class="card-text">${description}</p>
//           <div class ="card-footer text-body-secondary">${startDate} - ${endDate}</div>
//         </div>
//       </div>
//     </div>
//     `;
//   }


// function formatDate(date) {
//     const newDate = new Date(date);
//     const year = newDate.getFullYear();
//     const month = newDate.getMonth();
//     // const day = newDate.getDay();
//     const day = String(newDate.getDate()).padStart(2, '0');
//     const formattedDate = `${month}/${day}/${year}`;
//     return formattedDate


  window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/states/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        // Figure out what to do when the response is bad
      } else {
        const data = await response.json();
        console.log(data)

        const selectTag = document.getElementById('state');
        for (let state of data.states) {
            console.log(selectTag)
            const optionElement = document.createElement('option')
            optionElement.value = state.abbreviation
            optionElement.innerHTML = state.name
            selectTag.append(optionElement)
          }

        const formTag = document.getElementById('create-location-form');
        formTag.addEventListener('submit', async event => {
            event.preventDefault();
            const formData = new FormData(formTag);
            const json = JSON.stringify(Object.fromEntries(formData));

            const locationUrl = 'http://localhost:8000/api/locations/';
            const fetchConfig = {
                method: "post",
                body: json,
                headers: {
                    'Content-Type': 'application/json',
                },
            };
            const response = await fetch(locationUrl, fetchConfig);
            if (response.ok) {
                formTag.reset();
                const newLocation = await response.json();
                console.log(newLocation);
            }
        });

      }
    } catch (e) {
        console.error(e);
      // Figure out what to do if an error is raised
    }

  });
