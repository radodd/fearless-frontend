function createCard(name, description, pictureUrl, startDate, endDate, locationDetail) {
    return `
    <div class="col">
      <div class="card">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <p class="text-body-secondary text-muted">${locationDetail}</p>
          <p class="card-text">${description}</p>
          <div class ="card-footer text-body-secondary">${startDate} - ${endDate}</div>
        </div>
      </div>
    </div>
    `;
  }


function formatDate(date) {
    const newDate = new Date(date);
    const year = newDate.getFullYear();
    const month = newDate.getMonth();
    // const day = newDate.getDay();
    const day = String(newDate.getDate()).padStart(2, '0');
    const formattedDate = `${month}/${day}/${year}`;
    return formattedDate
}

  window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        // Figure out what to do when the response is bad
      } else {
        const data = await response.json();

        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const title = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const startDate = formatDate(details.conference.starts);
            const endDate = formatDate(details.conference.ends);
            console.log(startDate)
            console.log(endDate)
            const locationDetail = details.conference.location.name;
            const html = createCard(title, description, pictureUrl, startDate, endDate, locationDetail);
            const column = document.querySelector('.row');
            column.innerHTML += html;
          }
        }

      }
    } catch (e) {
        console.error(e);
      // Figure out what to do if an error is raised
    }

  });
